"""
This module is quite excellent for any number of testing purposes.
"""


def myfunction(variable: int) -> int:
    """this function is very useful"""
    return variable * 3 + 2


def myotherfunction(variable: int) -> int:
    """this function is even more useful"""
    return variable - 1


def too_complicated() -> None:
    """this function will increase code complexity"""
    for i in range(1000):
        print("was this really necessary??")
    print("I guess it was")
