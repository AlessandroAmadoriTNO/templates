"""
I can even make docstrings here! I'm awesome.
"""

from mysetuptoolspackage import mymodule


def test_myfunction():
    """Testing always makes me joyful."""
    assert mymodule.myfunction(variable=3) == 11
