---
title: Just CI
subtitle: Release v47.11.2
author:
  - R.H. ten Hove
  - F. Falconieri
date: 1 June 2030
titlepage: true
titlepage-color: "FFFFFF"
titlepage-text-color: "000000"
titlepage-rule-color: "000000"
titlepage-rule-height: 2
...

\newpage
