#!/usr/bin/env sh

if [ "${1}" = "" ]; then
    TARGET=${PWD}
else
    TARGET=${1}
fi

if command -v prettier > /dev/null; then
    prettier --write --ignore-unknown --prose-wrap always ${TARGET}
elif command -v docker > /dev/null; then
    docker run -u $(id -u):$(id -g) -v "${TARGET}:${TARGET}" registry.gitlab.com/just-ci/images/prettier:latest prettier --write --ignore-unknown --prose-wrap always ${TARGET}
else
    echo "[!] prettier nor docker were found. Cannot run."
fi
