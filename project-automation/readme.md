# Project Automation

## Pipeline Scheduler

Add multiple variables using a multiline variable.

```yaml
variables:
  SCHEDULE_PIPELINE_VARIABLES: |
    VARIABLE_KEY=VARIABLE_VALUE
    VARIABLE_KEY2=VARIABLE_VALUE2
```
