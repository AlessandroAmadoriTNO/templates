#!/usr/bin/env sh

set -e

cd "$(dirname "$0")/../.."

if ! command -v docker >/dev/null && ! command -v prettier >/dev/null; then
    echo "[!] You need either 'prettier' or 'docker' on PATH to run this script."
    exit 1
fi

for FILE in $(find . -type f ! -path "*/tests/*" -name "*.py"); do
    NAME_ONLY="${FILE%.*}"
    echo "[*] Fixing ${NAME_ONLY}..."
    JOB_FILE="${NAME_ONLY}.yml"

    if ! test -f "${JOB_FILE}"; then
        echo "[!] ${JOB_FILE} does not exist."
        continue
    fi

    black "${FILE}"
    isort --profile=black "${FILE}"

    echo 'python - << EOF' > /tmp/script.yaml
    cat "${FILE}" >> /tmp/script.yaml
    echo 'EOF' >> /tmp/script.yaml

    SCRIPT="$(cat /tmp/script.yaml)" yq -i '(..|select(has("script")).["script"].[0]) |= strenv(SCRIPT)' "${JOB_FILE}"

    docs/prettier.sh "${JOB_FILE}"
done
